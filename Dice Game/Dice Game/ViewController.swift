//
//  ViewController.swift
//  Dice Game
//
//  Created by Wylie, Louise on 27/11/2020.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var inputBox: UITextField!
    
    @IBOutlet weak var outputMessage: UILabel!
    
    @IBAction func guessButton(_ sender: Any) {
        self.view.endEditing(true)
        let diceRoll = Int.random(in: 2..<13)
        var guessAnswer = ""
        if Int(inputBox.text!) == diceRoll {
            guessAnswer = "correct"
        } else {
            guessAnswer = "incorrect"
        }
        let diceString = String(diceRoll)
        let theMessage = "Your guess of " + inputBox.text! + " is " + guessAnswer + ". The dice rolled " + diceString + "."
        outputMessage.text = theMessage
        inputBox.text! = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

